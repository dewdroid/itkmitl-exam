number = int(input())

print("-" * 15)
for i in range(2, number + 1):
    for j in range(1, 13):
        print("%d x %d = %d" % (i, j, i * j))
    print("-" * 15)